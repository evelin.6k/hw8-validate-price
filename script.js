
let form = document.createElement("form");
form.innerText = "Price";
form.className = "form";

let input = document.createElement('input');
input.className = "input";
input.type = "number"

form.append(input);
document.body.append(form);

let buttonCancel = document.createElement('button');
buttonCancel.innerText = "X";
buttonCancel.className= "button-cancel";

let inputSpan = document.createElement('span');
inputSpan.className = "input-span";

let spanError = document.createElement("span");
spanError.innerText = "Please enter correct price";
spanError.className = "span-error";

input.addEventListener("focus",() => input.classList.add("focus"))

input.addEventListener("blur", () => {
    input.classList.remove("focus");
    spanError.remove();

    if(input.value !== "" ){
        inputSpan.innerText = `Текущая цена: ${input.value} `;
        inputSpan.append(buttonCancel);
        form.before(inputSpan);
        input.style.backgroundColor = "rgba(0,255,0,0.5)";
        input.style.borderColor = ""
    }
    if(input.value < 0){
        input.style.backgroundColor = ""
        input.style.borderColor = "red";  
        inputSpan.remove();
        form.after(spanError)
    }
})

buttonCancel.addEventListener("click", () => {
inputSpan.remove();
input.style.backgroundColor = "";
input.value = "";

})
